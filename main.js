var roleHarvester = require('role.harvester');
var roleUpgrader = require('role.upgrader');
var roleBuilder = require('role.builder');
var roleDefender = require('role.defender');
var roleRepairer = require('role.repairer');
var autospawn = require('autospawn');
var roleExtHarvester = require('role.extHarvester');

global.max_defenders = 0;
global.max_harvesters = 2;
global.max_extHarvesters = 2;
global.max_upgraders = 2;
global.max_builders = 3;
global.min_upgraders = 1;
global.max_repairers = 0;

var creepsShould = max_defenders +
                    max_harvesters +
                    max_extHarvesters +
                    max_upgraders +
                    max_builders +
                    max_repairers;
//main loop
module.exports.loop = function () {
    var creepsHave = _.filter(Game.creeps).length
    if(creepsHave < creepsShould){
      autospawn.run();
    }


    console.log('----------------------------------------------------------')
    console.log(creepsHave + ' : ' + creepsShould)

    // -------------- TOWER -----------------------------
    var tower = Game.getObjectById('5ced0158781131403bec8dd8');
    if(tower) {
        var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => structure.hits < structure.hitsMax && structure.structureType === STRUCTURE_ROAD
        });
        if(closestDamagedStructure) {
            tower.repair(closestDamagedStructure);
        }
        // TOEWR Repair Wall
        var closestDamagedWall = tower.pos.findClosestByRange(FIND_STRUCTURES, {
            filter: (structure) => structure.hits < 300000 && structure.structureType === STRUCTURE_WALL
        });
        if(closestDamagedWall) {
            tower.repair(closestDamagedWall);
        }
        var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
        if(closestHostile) {
            tower.attack(closestHostile);
            console.log("--- !! UNDER ATTACK !! ---")
            global.max_defenders = 100;
        }
        else{
          global.max_defenders = 0;

        }
    }
    var amount = [];
    global.roles = ["harvesters", "upgraders", "builders", "extHarvesters", "defenders", "repairers"];
    for (i = 0; i < roles.length; i++) {
      var num = _.filter(Game.creeps, (creep) => creep.memory.role == roles[i]);
      amount[i] = num.length;
      //console.log(roles[i] + ': ' + amount[i]);
    }
    // -------- ACTIVATE CREEPS -------------------------
    for(var name in Game.creeps) {
        var creep = Game.creeps[name];
        if(creep.memory.role == 'harvesters') {
            roleHarvester.run(creep);
        }
        if(creep.memory.role == 'extHarvesters') {
            roleExtHarvester.run(creep);
        }
        if(creep.memory.role == 'upgraders') {
            roleUpgrader.run(creep);
        }
        if(creep.memory.role == 'builders') {
            roleBuilder.run(creep);
        }
        if(creep.memory.role == 'defenders') {
            roleDefender.run(creep);
        }
        if(creep.memory.role == 'repairers') {
            roleRepairer.run(creep);
        }
    }
}
