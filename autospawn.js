var autospawn = {
      run: function(spawn) {
        // ---------------------- COUNT CREEPS ------------------------------------------
      /**  var max_harvesters = 2;
        var max_extHarvesters = 2;
        var max_upgraders = 2;
        var min_upgraders = 1;
        var max_builders = 3;
        var max_repairers = 0;
        var max_defenders = 1; **/

        var energy = (Game.rooms['E9S16'].energyAvailable); //maximum Parts available
        console.log(energy);

        var home = 'E9S16';
        var target = 'E9S17';
        var sourceIndex = '0';

        var body_builders = [WORK, WORK, WORK, CARRY, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE];
        var body_repairers = [WORK, CARRY, MOVE];
        var body_harvesters = [WORK, CARRY, MOVE];
        var body_upgraders = [WORK, CARRY, MOVE];
        var body_extHarvesters = [WORK, CARRY, CARRY, CARRY, CARRY, CARRY, MOVE, MOVE];
        var body_defenders = [TOUGH, ATTACK, ATTACK, MOVE, MOVE, MOVE]
        var multi_move = 1;
        var multi_carry = 1;
        var multi_work = 1;
        //ensure there is enough parts
        //var parts = Math.round(max_parts/3)

        var numberOfParts = Math.floor(energy / 200);
        var body_all = [];
        for (let i = 0; i < numberOfParts; i++) {
          if(i < 3){
            body_all.push(WORK);
          }

        }
        for (let i = 0; i < numberOfParts; i++) {
          if(i < 4){
            body_all.push(CARRY);
          }

        }
        for (let i = 0; i < numberOfParts; i++) {
          if(i < 2){
            body_all.push(MOVE);
          }
        }
        console.log(body_all);

        var amount = [];
        var max_amount = [];
        var body = [body_harvesters, body_upgraders, body_builders, body_extHarvesters, body_defenders, body_repairers];
        var roles = ["harvesters", "upgraders", "builders", "extHarvesters", "defenders", "repairers"];
        var max_amount = [max_harvesters, max_upgraders, max_builders, max_extHarvesters, max_defenders, max_repairers];

        for (i = 0; i < roles.length; i++) {
          var num = _.filter(Game.creeps, (creep) => creep.memory.role == roles[i]);
          amount[i] = num.length;
          console.log(roles[i] + ": " + amount[i] + ' : ' + max_amount[i]);
          //-------- Checking that enough Upgraders are alive --------
          if(amount[1] < 1){
            max_amount = [0, 1, 0, 0, 0, 0];
            //max_builders = 0; max_harvesters = 0; max_extHarvesters = 0; max_defenders = 0; max_repaierers = 0;
          }
          //-------- In need of a Screep ------------------------------
          if(amount[i] < max_amount[i]){
            var newName = roles[i] + Game.time;
            console.log('Spawning new: ' + newName);
            Game.spawns['Spawn1'].spawnCreep(body_all, newName,
                {memory: {role: roles[i],
                          working: true}});
          }
        }
        if(Game.spawns['Spawn1'].spawning) {
            var spawningCreep = Game.creeps[Game.spawns['Spawn1'].spawning.name];
            Game.spawns['Spawn1'].room.visual.text(
                '🛠️' + spawningCreep.memory.role,
                Game.spawns['Spawn1'].pos.x + 1,
                Game.spawns['Spawn1'].pos.y,
                {align: 'left', opacity: 0.8});
        }
      }
};

module.exports = autospawn;
