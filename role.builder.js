var roleBuilder = {

    /** @param {Creep} creep **/
    run: function(creep) {
        var builder_task = 'rest'
        if(creep.memory.building && creep.carry.energy == 0) {
            creep.memory.building = false;
            builder_task = 'harvest';
            creep.say('🔄 harvest');
        }
        if(!creep.memory.building && creep.carry.energy == creep.carryCapacity) {
            creep.memory.building = true;
            creep.memory.task = 'build';
            creep.say('🚧 build');
        }


        //-------------------- BUILD -----------------------------
        if(creep.memory.building) {
          if(Game.rooms['E9S16'].energyAvailable < 300){
            console.log('Low on Energy -> Builder going to Harvest')
            var targets_harv = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
                }
            });
            if(targets_harv.length > 0) {
                if(creep.transfer(targets_harv[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets_harv[0], {visualizePathStyle: {stroke: '#ffffff'}});
                }
            }
          }
          else{
           //------------------ BUILD ----------------------------
            var targets = creep.room.find(FIND_CONSTRUCTION_SITES);
            if(targets.length) {
                if(creep.build(targets[0]) == ERR_NOT_IN_RANGE) {
                    creep.moveTo(targets[0], {visualizePathStyle: {stroke: '#ffffff'}});
                }
            }
            //------------------- FILL ---------------------------
            else{
              var targets_harv = creep.room.find(FIND_STRUCTURES, {
                  filter: (structure) => {
                      return (structure.structureType == STRUCTURE_EXTENSION ||
                          structure.structureType == STRUCTURE_SPAWN ||
                          structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
                  }
              });
              if(targets_harv.length > 0) {
                  if(creep.transfer(targets_harv[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                      creep.moveTo(targets_harv[0], {visualizePathStyle: {stroke: '#ffffff'}});
                  }
              }
          }
        }
      }

        //-------------------- HARVEST --------------------------------------
        else {
            var sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(sources[1]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[1], {visualizePathStyle: {stroke: '#ffaa00'}});
            }
        }
    }
};

module.exports = roleBuilder;
