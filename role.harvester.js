var roleHarvester = {

    /** @param {Creep} creep **/
    run: function(creep) {
      var tower = Game.getObjectById('5ced0158781131403bec8dd8');

      if(creep.memory.working && creep.carry.energy == 0) {
          creep.memory.working = false;
          creep.say('🔄 harvest');
      }
      if(!creep.memory.working && creep.carry.energy == creep.carryCapacity) {
          creep.memory.working = true;
          creep.say('⚡ FILL');
      }




        if(!creep.memory.working) { // HARVESTING
            var sources = creep.room.find(FIND_SOURCES);
            if(creep.harvest(sources[0]) == ERR_NOT_IN_RANGE) {
                creep.moveTo(sources[0], {visualizePathStyle: {stroke: '#ffaa00'}});
            }
        }
        else { //FILLING
            var targets_harv = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_EXTENSION ||
                        structure.structureType == STRUCTURE_SPAWN ||
                        structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
                }
            });
            var target_towers = creep.room.find(FIND_STRUCTURES, {
                filter: (structure) => {
                    return (structure.structureType == STRUCTURE_TOWER) && structure.energy < structure.energyCapacity;
                }
            });
            //console.log(tower.energy + ' -- ' + tower.energyCapacity);
            if(Game.rooms['E9S16'].energyAvailable > Game.rooms['E9S16'].energyCapacityAvailable/3){
              if(tower.energy < (tower.energyCapacity/3)){
                if(target_towers.length > 0){
                  if(creep.transfer(target_towers[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                      creep.moveTo(target_towers[0], {visualizePathStyle: {stroke: '#ffffff'}});
                  }
                }
              }
              else{
                if(targets_harv.length > 0) {
                    if(creep.transfer(targets_harv[0], RESOURCE_ENERGY) == ERR_NOT_IN_RANGE) {
                        creep.moveTo(targets_harv[0], {visualizePathStyle: {stroke: '#ffffff'}});
                    }
                }
              }
            }  



            if(Game.rooms['E9S16'].energyCapacityAvailable == Game.rooms['E9S16'].energyAvailable){
              var targets_build = creep.room.find(FIND_CONSTRUCTION_SITES);
              if(targets_build.length) {
                  if(creep.build(targets_build[0]) == ERR_NOT_IN_RANGE) {
                      creep.moveTo(targets_build[0], {visualizePathStyle: {stroke: '#ffffff'}});
                  }
              }
            }
        }
    }
};

module.exports = roleHarvester;
