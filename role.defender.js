var roleDefender = {
  /**@param {Creep} creep**/
  run: function(creep) {
    var roomName = 'E9S16';
    const i = 0;
    var hostiles = Game.rooms[roomName].find(FIND_HOSTILE_CREEPS);
    const target = creep.pos.findClosestByRange(FIND_HOSTILE_CREEPS);
    if(hostiles.length > 0){
      var username = hostiles[0].owner.username;
      Game.notify(`User ${username} spotted in room ${roomName}`);
      console.log(`User ${username} spotted in room ${roomName}`)
      creep.moveTo(hostiles[i])
      creep.attack(hostiles[i])
    }
    else{
      creep.moveTo(20, 28); //resting location
    }
  }
}
module.exports = roleDefender;
